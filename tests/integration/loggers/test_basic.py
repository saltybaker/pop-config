def test_basic(logfile, cli):
    cli(
        "--log-plugin=basic",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "mode=a",
    )
    contents = logfile.read_text()
    assert "[TRACE   ] trace\n" in contents


def test_async(logfile, cli):
    cli(
        "--log-plugin=async",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "mode=a",
    )
    contents = logfile.read_text()
    assert "[TRACE   ] trace\n" in contents


def test_null(cli):
    stderr = cli(
        "--log-plugin=null",
        "--log-level=trace",
    )
    assert "[TRACE   ] trace\n" in stderr
