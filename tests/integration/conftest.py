import pop.hub
import pytest


@pytest.fixture(scope="function", name="hub")
def integration_hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")
    yield hub
