import os
import pathlib
import subprocess
import sys

import pytest


def run_cli(*args, env=None):
    if env is None:
        env = {}

    if os.name == "nt" and "SYSTEMROOT" not in env:
        env["SYSTEMROOT"] = os.getenv("SYSTEMROOT")

    runpy = pathlib.Path(__file__).parent / "run.py"
    command = [sys.executable, str(runpy), *args]

    proc = subprocess.Popen(command, encoding="utf-8", env=env, stdout=subprocess.PIPE)
    assert proc.wait() == 0

    # We only care about what was printed to the logs
    return proc.stdout


@pytest.fixture(name="cli", scope="function")
def cli():
    return run_cli
