import os
import unittest.mock as mock

import pop.hub
import pytest


@pytest.mark.skipif(not hasattr(os, "geteuid"), reason="This test requires os.geteuid")
def test_root_dir_noop():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")
    with mock.patch(
        "pop_config.config.contracts.order.os.geteuid",
        autospec=True,
        return_value=0,
    ):
        hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT.r1.cheese_dir == "/etc/r1/cheese"
    assert hub.OPT.r1.bacon_dir == "/etc/foo/bacon"


@pytest.mark.skipif(not hasattr(os, "geteuid"), reason="This test requires os.geteuid")
def test_root_dir_explicit():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")
    with mock.patch(
        "sys.argv",
        [
            "config",
            "--root-dir",
            "myroot",
        ],
    ):
        with mock.patch(
            "pop_config.config.contracts.order.os.geteuid",
            autospec=True,
            return_value=0,
        ):
            hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT.r1.cheese_dir == "myroot/etc/r1/cheese"
    assert hub.OPT.r1.bacon_dir == "/etc/foo/bacon"


@pytest.mark.skipif(not hasattr(os, "geteuid"), reason="This test requires os.geteuid")
def test_root_dir_non_root_user():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")
    with mock.patch(
        "pop_config.config.contracts.order.os.geteuid",
        autospec=True,
        return_value=12345,
    ):
        hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT.r1.root_dir != "/"
