import gc
import logging

import pop.hub


def test_log_handler_cleanup():
    root = logging.getLogger()
    handlers_len = len(root.handlers)

    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")
    hub.config.integrate.load("s1", "s1")
    assert len(root.handlers) > handlers_len

    del hub
    gc.collect()
    assert handlers_len == len(root.handlers)
