.. pop-config documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _README:

.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :glob:
   :hidden:

   topics/about
   topics/introduction
   topics/config
   topics/cli_config
   topics/subcommands
   topics/advanced
   topics/merging
   topics/versions_report
   releases/index

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:
   :hidden:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/pop/pop-config/>

.. toctree::
   :caption: Links
   :hidden:

   POP Project Source on GitLab <https://gitlab.com/vmware/pop>
   Idem Project Docs Portal <https://docs.idemproject.io>
   Idem Project Website <https://www.idemproject.io>
   Idem Project Source on GitLab <https://gitlab.com/vmware/idem>
